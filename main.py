import os.path
import sys
import pandas as pd
import re
import time


class LogParser:
    """
    This class contains all the logic and requested functions for parsing openvpn log files.
    Except clauses are in place to combat human error.
    """
    def __init__(self):
        self.LINES = None

        # Check config file exists
        if not os.path.isfile("config.xml"):
            with open("config.xml", "w") as f:
                f.write("openvpn.log")

        # Read config file for path
        with open("config.xml") as f:
            path = f.readline()

        # Try reading .log file at path
        try:
            with open(path) as f:
                self.LINES = f.readlines()
        except FileNotFoundError:
            print(f"Sorry log file: {path} not found.")
            sys.exit(1)
        except PermissionError:
            print(f"Sorry it looks like this program doesn't have the necessary permissions to access file: {path}")
            sys.exit(1)
        except Exception as e:
            print(f"Sorry an unexpected error has occurred: {e}")
            sys.exit(1)

    def sort_list(self, string):
        lines = self.LINES
        # Retain only elements that contain string
        lines = [lines for lines in lines if string in lines]
        return lines

    def regex_find(self, regex, lines):
        # Conjoin list into single string for re.findall
        lines = "".join(lines)
        regex_list = re.findall(regex, lines)
        return regex_list

    def top_days(self):
        log_lines = self.sort_list("TLS: Initial packet")
        # Retain only date
        log_lines = [line[:10] for line in log_lines]
        # Calc top 5 days and amount
        df = pd.DataFrame(log_lines, columns=["Top 5 Dates:"]).value_counts().head(5)
        return df

    def amount_non_ovpn(self):
        log_lines = self.sort_list("Non-OpenVPN client protocol detected")
        return f"There were {len(log_lines)} connections that did not comply with the Open VPN protocol."

    def top_successful_connections(self):
        log_lines = self.sort_list("TLS: Initial packet")
        ip_list = self.regex_find(r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}', log_lines)
        # Calc top 10 IP and amount
        df = pd.DataFrame(ip_list, columns=["Top 10 IP Addresses:"]).value_counts().head(10)
        return df

    def executed_management_commands(self):
        log_lines = self.sort_list("MANAGEMENT: CMD")
        management_list = self.regex_find(r"'(.*?)'", log_lines)
        # Calc top commands
        df = pd.DataFrame(management_list, columns=["Management Commands that have been executed:"]).value_counts()
        return df


class Interface:
    """
    This class contains everything to put the logic in the 'LogParser' class to use. It will construct a multiple choice
    menu when running in normal 'cli' mode or give the output of all functions when given the '-h' (headless) argument.
    """
    def __init__(self):
        self.lp = LogParser()

    def cli(self):
        print("""
        Welcome to the OpenVPN log parser. Please make a choice from the following commands:
        1. Show an overview of the top five days where the most successful connections where made.
        2. Show the amount of connections that were made that do not comply with the Open VPN protocol.
        3. Show an overview of the top ten IP addresses that were successfully connected to the Open VPN server.
        4. Show an overview of executed management commands.
        5. Exit
        """)

        choice = input("Make a choice: ")
        if choice == "1":
            print(self.lp.top_days())
            time.sleep(1)
        elif choice == "2":
            print(self.lp.amount_non_ovpn())
            time.sleep(1)
        elif choice == "3":
            print(self.lp.top_successful_connections())
            time.sleep(1)
        elif choice == "4":
            print(self.lp.executed_management_commands())
            time.sleep(1)
        elif choice == "5":
            sys.exit(0)
        else:
            print("Sorry that is an invalid choice.")
            time.sleep(1)

    def headless(self):
        print(f"""
        Showing output of all commands:
        1: {self.lp.top_days()} \n
        2: {self.lp.amount_non_ovpn()} \n
        3: {self.lp.top_successful_connections()} \n
        4: {self.lp.executed_management_commands()}
        """)


if __name__ == "__main__":
    interface = Interface()
    try:
        if sys.argv[1] == "-h":
            interface.headless()
        else:
            print(f"Sorry, '{sys.argv[1]}' is not a valid argument. Use '-h' to run in headless mode.")
    # If not given an argument sys.argv[1] throws IndexError. When not given an argument construct 'normal' cli.
    except IndexError:
        while True:
            interface.cli()
    except Exception as e:
        print(f"Sorry an unexpected error has occurred: {e}")
        sys.exit(1)
