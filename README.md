<h1 align="center">IPASS-Programming</h1>
<h3 align="center">OpenVPN log parser</h3>

## Overview:
This program allows you to view useful stats from your openvpn.log file (different functions described below).
This program was made for my IPASS at the HU University of Applied Sciences. It has all the necessary and extra functions
that were desired by the HU. I strongly desired to go the OOP way of using classes. Even though we didn't learn about 
them in class. I also strongly advice you to do the same with your Python project(s). It looks (and is) cleaner and at 
the end of the day it will speed up your coding. If you're stumbling upon this project I hope it could be useful to you :)

## Functions:
- Show an overview of the top five days where the most successful connections where made.
- Show the amount of connections that were made that do not comply with the Open VPN protocol.
- Show an overview of the top ten IP addresses that were successfully connected to the Open VPN server.
- Show an overview of executed management commands.

## Arguments:
The program allows for the '-h' argument to be used. If given the '-h' argument, the program will run in 'headless' mode.
This means that it will display the output of all functions above. You could for example save this output to a text file 
by adding '> example.txt' at the end of 'python main.py'.

## Requirements
This script uses standard Python libraries and pandas. Install pandas by running `python3 -m pip install pandas`.
